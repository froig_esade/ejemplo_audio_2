//
//  ViewController.swift
//  Exemple audio
//
//  Created by Francesc Roig.
//  Revisión: 22/04/2018.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {

    // Componente reproductor de audio.
    @objc var player1: AVAudioPlayer!
    @objc var player2: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        cargaAudio(nombre: "The Crab Apples - Right Here", player: &player1)
        cargaAudio(nombre: "The Crab Apples - Three II", player: &player2)
    }


    
    // Acción boton "Reproduce canción 1".
    @IBAction func accionBotonReproduce1(_ sender: Any) {
        
        player1.play()
    }

    
    
    // Acción boton "Reproduce canción 2".
    @IBAction func accionBotonReproduce2(_ sender: Any) {
        
        player2.play()
    }
    
    
    
    // Acción boton "Stop canción 1".
    @IBAction func accionBotonStop1(_ sender: Any) {
        
        player1.stop()
    }
    
    
    // Acción boton "Stop canción 2".
    @IBAction func accionBotonStop2(_ sender: Any) {
        
        player2.stop()
    }
    
    
    
    // Función que carga el audio seleccionado.
    func cargaAudio(nombre:String, player: inout AVAudioPlayer!)  {
        
        guard let sound = NSDataAsset(name: nombre) else {
            print("Audio no encontrado!!")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(data: sound.data)
        } catch let error as NSError {
            print("Error de reproducción: \(error.localizedDescription)")
        }
    }

   
    
    
    
} // Final bloque ViewController

